<?php

class User
{
    private $db;

    public $uname;
    public $upass;

    /*
    function __construct($dbc)
    {
        $this->db = $dbc;
    }
    */
    public function register($uname, $uemail, $upass)
    {
        if (!filter_var($uemail, FILTER_VALIDATE_EMAIL))
        {
            $emailErr = "Invalid email format";
        }

        $new_pass = password_hash($upass, PASSWORD_DEFAULT);

        $stmt=$this->db->prepare("INSERT INTO users (user_name, user_email, user_pass) VALUES (:uname, :uemail, :upass)");

        $stmt->bindparam(":uname", $uname);
        $stmt->bindparam(":uemail", $umail);
        $stmt->bindparam(":upass", $new_pass);
        $stmt->execute();

        return $stmt;

        //include("./app/view/register.php");
    }

    public function login($uname, $upass)
    {
        $hashed_password = "hASH Z BAZY";
        password_verify($upass, $hashed_password);

        /*
        if the password match it will return true.
        */
    }

    public function isLogged()
    {
        if(isset($_SESSION['UserId']))
        {
            return true;
        }
    }

    public function logout()
    {
        session_destroy();
        unset($_SESSION['UserId']);
    }

    public function delete()
    {

    }

    public function update()
    {

    }
}
