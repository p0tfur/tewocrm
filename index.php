<?php

require_once('./app/config.php');
require_once(APP_ROOT.'/autoloader.php');

session_start();

$func = new Functions();
$obj2 = new dbconnection();

require_once ('./app/view/header.php');

$url = array();
$url = explode("/", filter_input(INPUT_SERVER, 'REQUEST_URI')); //regex?
//$url=explode("/", $_SERVER['REQUEST_URI']);
switch ($url[2])
{
  case "main":
    if($url[3]===""){include "./app/view/main.php";}//+template
    elseif($url[3]==="faq"){include "./app/view/faq.php";}
    elseif($url[3]==="contact"){include "./app/view/contact.php";}
  break;
  case "user":
    $user = new User();
    if($url[3]==="login"){include "./app/view/login.php";}
    elseif($url[3]==="register"){include "./app/view/register.php";}
    elseif($url[3]==="logout"){include "./app/view/logout.php";}
  break;
  default:
    include "./app/view/404.php";
  break;
}

require_once ('./app/view/footer.php');
